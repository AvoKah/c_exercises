CC=clang
CFLAGS=-Wextra -Wall -Werror -pedantic -std=c99 -g
SRC=fifo/fifo.c bst/bst.c stack/stack.c
SRC:=$(addprefix src/,$(SRC))
SRC+=check/main.c
DEL= *.o *~ .*.swp *.so *.class *.log *.a *.dot *.png
BIN=bin

all:$(BIN)

$(BIN): $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o $(BIN)
clean:
	rm -rf $(DEL) $(BIN)
