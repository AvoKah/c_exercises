#include "../src/fifo/fifo.h"
#include "../src/bst/bst.h"
#include "../src/stack/stack.h"

void fifo_test()
{
    fifo *f = fifo_init();
    fifo_push(f, 1);
    fifo_push(f, 2);
    fifo_push(f, 3);
    fifo_push(f, 4);
    fifo_push(f, 5);
    fifo_push(f, 6);
    fifo_push(f, 18);
    fifo_push(f, 17);

    fifo_print(f);
    printf("\nfifo_pop 2 times and pushing 7\n\n");
    fifo_pop(f);
    fifo_pop(f);

    fifo_push(f, 7);
    fifo_print(f);

    fifo_destroy(f);
}

void stack_test()
{
    stack *s = stack_init();

    stack_push(s, 1);
    stack_push(s, 2);
    stack_push(s, 3);
    stack_push(s, 4);
    stack_push(s, 5);
    stack_push(s, 6);
    stack_push(s, 9);

    printf("Stack size : %d\nLast item : %d\n", (int)s->size, s->head->elt);
    printf("Pop.\n");

    stack_pop(s);
    printf("Stack size : %d\nLast item : %d\n", (int)s->size, s->head->elt);
    stack_destroy(s);
}

void bst_test()
{
    bst *b = bst_init();
    bst_insert(b, 15, "le");
    bst_insert(b, 20, "chocolat");
//    bst_insert(b, 10, "j'aime");
//    bst_insert(b, 20, "nutella");
//    bst_insert(b, 13, "pas");
//
//    bst_print(b);
//    b = bst_remove(b, 13);
//    bst_print(b);
//    bst_insert(b, 20, "chocolat");
//    bst_insert(b, 25, "noir");
//    bst_print(b);
//
//    bst_show(b);
    bst_destroy(b);
}

int main(void)
{
    fifo_test();
    stack_test();
    bst_test();
    return 0;
}
