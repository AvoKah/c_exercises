#include "bst.h"
#include <unistd.h>

bst *bst_init(void)
{
    bst *b = malloc(sizeof (bst));
    b->left = NULL;
    b->right = NULL;
    b->node = NULL;
    return b;
}

void bst_insert(bst *b, int key, char *value)
{
    if (b->node == NULL)
    {
        b->node = malloc(sizeof (node));
        b->node->key = key;
        b->node->value = value;
        b->left = NULL;
        b->right = NULL;
    }
    else if (b->node->key == key)
        b->node->value = value;
    else if (key < b->node->key)
    {
        if (b->left == NULL)
        {
            b->left = bst_init();
            b->left->node = malloc(sizeof (node));
            b->left->node->key = key;
            b->left->node->value = value;
        }
        else
            bst_insert(b->left, key, value);
    }
    else
    {
        if (b->right == NULL)
        {
            b->right = bst_init();
            b->right->node = malloc(sizeof (node));
            b->right->node->key = key;
            b->right->node->value = value;
        }
        else
            bst_insert(b->right, key, value);
    }
}

char *bst_find(bst *b, int key)
{
    if (!b)
        return "";
    if (b->node->key == key)
        return b->node->value;
    else
    {
        if (key < b->node->key)
            return bst_find(b->left, key);
        else
            return bst_find(b->right, key);
    }
}

//static void free_bst(bst *b)
//{
//    if (b->right)
//        free_bst(b->right);
//    if (b->left)
//        free_bst(b->left);
//    b->node = NULL;
//    free(b->node);
//    b = NULL;
//    free(b);
//}

bst *bst_remove(bst *b, int key)
{
    if (!b->left && !b->right)
    {
        free(b->node);
        free(b);
        return NULL;
    }
    bst *tmp = b;
    bst *savetmp = b;
    int lr = 0;
    while (tmp && tmp->node->key != key)
    {
        savetmp = tmp;
        if (key < tmp->node->key)
        {
            tmp = tmp->left;
            lr = -1;
        }
        else
        {
            tmp = tmp->right;
            lr = 1;
        }
    }
    if (tmp)
    {
        if (!tmp->left && !tmp->right)
        {
            if (lr == 1)
                savetmp->right = NULL;
            else if (lr == -1)
                savetmp->left = NULL;
            free(tmp->node);
        }
        else if (!tmp->left)
        {
            if (lr == 1)
                savetmp->right = tmp->right;
            else if (lr == -1)
                savetmp->left = tmp->right;
            else
                b = tmp->right;
            tmp->right = NULL;
            free(tmp->node);
        }
        else if (!tmp->right)
        {
            if (lr == 1)
                savetmp->right = tmp->left;
            else if (lr == -1)
                savetmp->left = tmp->left;
            else
                b = tmp->left;
            tmp->left = NULL;
            free(tmp->node);
        }
        else
        {
            savetmp = tmp;
            tmp = tmp->right;
            lr = 0;
            while (tmp->left)
            {
                tmp = tmp->left;
                lr = -1;
            }
            if (!tmp->right)
            {
                savetmp->node = tmp->node;
                tmp->node = NULL;
                free(tmp->node);
            }
            else
            {
                if (!lr)
                {
                    savetmp->right = tmp->right;
                    tmp->right = NULL;
                    free(tmp->node);
                }
                else
                {
                    savetmp->node = tmp->node;
                    savetmp = savetmp->right;
                    while (savetmp->left && savetmp->left->left)
                        savetmp = savetmp->left;
                    savetmp->left = tmp->right;
                    free(tmp->node);
                }
            }
        }
    }
    tmp = NULL;
    free(tmp);
    savetmp = NULL;
    free(savetmp);
    return b;
}

void bst_destroy(bst *b)
{
    while (b && b->node != NULL)
        b = bst_remove(b, b->node->key);
    free(b);
}


static void bst_print_rec(bst *b)
{
    if (!b)
        return;
    if (b->left != NULL)
        bst_print_rec(b->left);
    printf("%s ", b->node->value);
    if (b->right != NULL)
        bst_print_rec(b->right);
}

void bst_print(bst *b)
{
    bst_print_rec(b);
    printf("\n");
}

static void bst_show_rec(FILE *f, bst *b)
{
    if (!b)
        return;
    int printed = 0;
    if (b->left != NULL)
    {
        fprintf(f, "\"%s\"", b->node->value);
        printed = 1;
        fprintf(f, " -- ");
        bst_show_rec(f, b->left);
    }
    if (b->right != NULL)
    {
        fprintf(f, "\"%s\"", b->node->value);
        printed = 1;
        fprintf(f, " -- ");
        bst_show_rec(f, b->right);
    }
    if (!printed)
        fprintf(f, "\"%s\"", b->node->value);
    fprintf(f, "\n");
}

void bst_show(bst *b)
{
    FILE *dotfile = fopen("bst.dot", "w+");
    fprintf(dotfile, "graph ast {\n");
    bst_show_rec(dotfile, b);
    fprintf(dotfile, "}\n");
    fclose(dotfile);
    execlp("dot", "dot", "-Tpng", "bst.dot", "-o", "bst.png", NULL);
}
