#ifndef BST_H
# define BST_H

# include <stdlib.h>
# include <stdio.h>

typedef struct node
{
    int key;
    char *value;
} node;

typedef struct bst
{
    struct bst *left;
    struct bst *right;
    node *node;
} bst;

bst *bst_init();
void bst_insert(bst *b, int key, char *value);
char *bst_find(bst *b, int key);
bst *bst_remove(bst *b, int key);
void bst_print(bst *b);

void bst_destroy(bst *b);

/**
 * Creates Dotfile that shows the actual bst
 */
void bst_show(bst *b);

#endif //BST_H
