#ifndef LIST_H
# define LIST_H

typedef struct list
{
    int elt;
    struct list *next;
} list;

#endif //!LIST_H
