#include "stack.h"

stack *stack_init()
{
    stack *s = malloc(sizeof (stack));
    s->head = NULL;
    s->size = 0;
    return s;
}

int stack_empty(stack *s)
{
    return (s->size == 0);
}

void stack_push(stack *s, int elt)
{
    list *l = malloc(sizeof (list));
    l->elt = elt;
    l->next = s->head;
    s->head = l;
    s->size++;
}

int stack_pop(stack *s)
{
    int elt = s->head->elt;
    list *tmp = s->head;
    s->head = tmp->next;
    s->size--;
    free(tmp);
    return elt;
}

void stack_clear(stack *s)
{
    while (s->size)
        stack_pop(s);
}

void stack_destroy(stack *s)
{
    stack_clear(s);
    free(s);
}
