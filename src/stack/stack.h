#ifndef STACK_H
# define STACK_H

# include <stdlib.h>

# include "../list.h"

typedef struct stack
{
    list *head;
    size_t size;
} stack;

stack *stack_init();
int stack_empty(stack *s);
void stack_push(stack *s, int elt);
int stack_pop(stack *s);
void stack_clear(stack *s);

void stack_destroy(stack *s);

#endif //STACK_H
