#ifndef FIFO_H
# define FIFO_H

# include <stdlib.h>
# include <stdio.h>

# include "../list.h"

typedef struct
{
    list *head;
    list *tail;
    size_t size;
} fifo;

fifo *fifo_init();
int fifo_empty(fifo *f);
void fifo_push(fifo *f, int elt);
int fifo_pop(fifo *f);
void fifo_clear(fifo *f);

void fifo_destroy(fifo *f);
void fifo_print(fifo *f);

#endif //FIFO_H
