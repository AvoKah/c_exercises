#include "fifo.h"
#include <limits.h>

fifo *fifo_init(void)
{
    fifo *f = malloc(sizeof (fifo));
    f->head = NULL;
    f->tail = NULL;
    f->size = 0;
    return f;
}

int fifo_empty(fifo *f)
{
    return (f->size == 0);
}

void fifo_push(fifo *f, int elt)
{
    list *l = malloc(sizeof (list));
    l->elt = elt;
    l->next = NULL;
    if (f->size == 0)
        f->head = l;
    else
        f->tail->next = l;
    f->tail = l;
    f->size++;
}

int fifo_pop(fifo *f)
{
    if (f->size > 0)
    {
        int ret = f->head->elt;
        list *tmp = f->head;
        f->head = tmp->next;
        f->size--;
        free(tmp);
        return ret;
    }
    return INT_MAX;
}

void fifo_clear(fifo *f)
{
    while (f->size)
        fifo_pop(f);
}

void fifo_destroy(fifo *f)
{
    fifo_clear(f);
    free(f);
}

void fifo_print(fifo *f)
{
    if (f->size == 0)
        printf("Empty fifo");
    else if (f->size == 1)
        printf("%d", f->head->elt);
    else
    {
        list *tmp = f->head;
        while (tmp->next)
        {
            printf("%d <- ", tmp->elt);
            tmp = tmp->next;
        }
        printf("%d", f->tail->elt);
    }
    printf("\n");
}
